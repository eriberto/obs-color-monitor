// Thanks to Marcos Talau, 2021

#include <iostream>
#include <dlfcn.h>

using namespace std;

int main(int argc, char **argv) {
    void* handle;

    if (argc != 2) {
        cout << "Usage: " << argv[0] << " some_lib.so" << endl;
        exit(1);
    }

    handle = dlopen(argv[1], RTLD_LAZY);

    if (!handle) {
        cout << "ERROR: " << dlerror() <<  endl;
        exit(1);
    }

    exit(0);
}
